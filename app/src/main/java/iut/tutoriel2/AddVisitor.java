package iut.tutoriel2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddVisitor extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_visitor);
        final EditText nom = findViewById(R.id.editTextNom);
        final EditText prenom = findViewById(R.id.editTextPrenom);
        Button btnConfirm = findViewById(R.id.confirmAddVisitor);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prenom.getText().length() != 0 && nom.getText().length() != 0) {
                    Intent intent = new Intent();
                    intent.putExtra("nom", nom.getText().toString());
                    intent.putExtra("prenom", prenom.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                } else
                    Toast.makeText(getApplicationContext(), "Missing Args", Toast.LENGTH_LONG).show();
            }
        });
    }
}
