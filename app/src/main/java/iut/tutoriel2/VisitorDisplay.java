package iut.tutoriel2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class VisitorDisplay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_display);
        final Invitation invit = getIntent().getParcelableExtra("invit");
        TextView nom = findViewById(R.id.NomDisplay);
        nom.setText(invit.nomInvite());
        TextView prenom = findViewById(R.id.PrenomDisplay);
        prenom.setText(invit.prenomInvite());
        final ToggleButton btnConfirm = findViewById(R.id.ButtonConfirm);
        btnConfirm.setChecked(invit.estConfirmee());
        Button btn = findViewById(R.id.outButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnConfirm.isChecked() != invit.estConfirmee()) {
                    Intent intent = new Intent();
                    Invitation newInvit = new Invitation(invit.nomInvite(), invit.prenomInvite());
                    newInvit.confirmer(btnConfirm.isChecked());
                    intent.putExtra("invit", newInvit);
                    intent.putExtra("position", getIntent().getIntExtra("position", 0));
                    setResult(RESULT_OK, intent);
                }
                finish();
            }
        });
        Button infoBtn = findViewById(R.id.infoButton);
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Intent.ACTION_WEB_SEARCH );
                intent.putExtra(SearchManager.QUERY, String.format("%s %s", invit.nomInvite(), invit.prenomInvite()));
                startActivity(Intent.createChooser(intent , "Open with ..."));
            }
        });

    }
}
