package iut.tutoriel2;

import java.util.ArrayList;

public class InvitationViewModel extends androidx.lifecycle.ViewModel {

    private ArrayList<Invitation> invits;

    public ArrayList<Invitation> getUsers() {
        if (invits == null) {
            invits = new ArrayList<>();
            invits.add(new Invitation("Vador", "Dark"));
            invits.add(new Invitation("Stark", "Tony"));
            Invitation as = new Invitation("Stark", "Arya");
            invits.add(as);
            as.confirmer(true);
        }
        return invits;
    }
}