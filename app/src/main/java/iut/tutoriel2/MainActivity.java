package iut.tutoriel2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    class myViewHolder extends RecyclerView.ViewHolder {

        public TextView fullName;
        public TextView initial;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            fullName = itemView.findViewById(R.id.fullName);
            initial = itemView.findViewById(R.id.initial);
        }
    }
    class myAdapter extends RecyclerView.Adapter<myViewHolder> {

        List<Invitation> invits;

        public myAdapter(List<Invitation> invits) {
            this.invits = invits;
        }

        @NonNull
        @Override
        public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Context ctx = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(ctx);
            View baseView = inflater.inflate(R.layout.invitation_line, parent, false);
            return new myViewHolder(baseView);
        }

        @Override
        public void onBindViewHolder(@NonNull myViewHolder holder, int position) {
            final Invitation invite = invits.get(position);
            TextView fullName = holder.fullName;
            fullName.setText(String.format("%s %s", invite.nomInvite(), invite.prenomInvite()));
            TextView initial = holder.initial;
            initial.setText(String.valueOf(invite.nomInvite().charAt(0)).toUpperCase());
            View itemView = holder.itemView;
            itemView.setBackgroundColor(invite.estConfirmee() ? Color.GREEN : Color.RED);
            itemView.setOnClickListener((view) -> {
                Intent intent = new Intent(MainActivity.this, VisitorDisplay.class);
                intent.putExtra("invit", invite);
                intent.putExtra("position", position);
                startActivityForResult(intent, FLAG_ACTIVITY_2);
            });
        }

        @Override
        public int getItemCount() {
            return invits.size();
        }

        public Invitation getItem(int position) {
            return this.invits.get(position);
        }

        public void sort(Comparator<Invitation> c) {
            Collections.sort(invits, c);
        }
    }
    final static int FLAG_ACTIVITY = 7545;
    final static int FLAG_ACTIVITY_2 = 7554;
    List<Invitation> invits = new ArrayList<>();
    myAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InvitationViewModel invitationViewModel = new ViewModelProvider(this).get(InvitationViewModel.class);
        invits = invitationViewModel.getUsers();
        Button btnAddVisitor = findViewById(R.id.addVisitor);
        btnAddVisitor.setOnClickListener((v) -> {
                Intent intent = new Intent(MainActivity.this, AddVisitor.class);
                startActivityForResult(intent, FLAG_ACTIVITY);
        });
        this.updateList();
        Button sendList = findViewById(R.id.sendList);
        sendList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, invits.toString());
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, "Send with ..."));

            }
        });
    }

    protected void updateList() {
        RecyclerView list = findViewById(R.id.list);
        adapter = new myAdapter(invits);
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter.sort(Invitation.COMPARATEUR_INVITES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == FLAG_ACTIVITY)
                invits.add(new Invitation(data.getStringExtra("nom"), data.getStringExtra("prenom")));
            if (requestCode == FLAG_ACTIVITY_2)
                invits.set(data.getIntExtra("position", 0), (Invitation) data.getParcelableExtra("invit"));
            this.updateList();
        }
    }
}
